class DetectIphone {
    constructor(){
        this.init();
    }

    init(){
        //Check if iphone or not
        var isIphone = /(iPhone)/i.test(navigator.userAgent);
        var isIpad = /(iPad)/i.test(navigator.userAgent);

        if(!isIphone){ 
            // checks if element is playing right now
            // Do anything you want to
            document.querySelector('html').classList.remove('isIphone');
        }
        else {
            document.querySelector('html').classList.add('isIphone');
        }

        if(!isIpad){
            // checks if element is playing right now
            // Do anything you want to
            document.querySelector('html').classList.remove('isIpad');
        }
        else {
            document.querySelector('html').classList.add('isIpad');
        }
    }
}

export default DetectIphone;
