# Right Side Up

Right Side Up marketing website codebase.


- [x] Webpack 4
- [x] Babel 7
- [x] JS Hot module replacement
- [x] Pug template engine
- [x] Scss style preprocessor
- [x] Scss Hot module replacement
- [x] Sass MQ